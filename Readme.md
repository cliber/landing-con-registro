# Solución para ejercicio landing con registro

Este repositorio contiene una solución para el reto de
[landing con registro](https://portafolio.cliber.io/retos/landing-con-registro-gtnjfs/)
propuesto en [Portafolio by Cliber](https://portafolio.cliber.io).

La solución propuesta consiste en un único archivo de html con todos los estilos
necesarios para darle forma al sitio propuesto.
